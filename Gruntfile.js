module.exports = function (grunt)
{
	// Project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
					{
						expand: true, 
						src: 'index.html',
						dest: 'build/'
					},
					{
						expand: true,
						src: 'js/EG.js',
						dest: 'build/js/'
					},
					{
						expand: true,
						src: 'css/EG.css',
						dest: 'build/'
					},
					{
						expand: true,
						src: 'css/vendor/bootstrap.css',
						dest: 'build/'
					}
				],
				options: {
					process: function (content, srcpath){
						return content.replace(/<script src = "http:\/\/localhost:9090\/livereload\.js"><\/script>/g, "");
					}
				}
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: 'js/EG.js',
				dest: 'build/prod/min.js/EG.js'
			}
		},
		jshint: {
			options: {
				globals: {
					jquery: true
				}
			},
			build: ['Gruntfile.js', 'js/EG.js']
		},
		less: {
			compileCore: {
				options: {
					strictMath: true,
					sourceMap: true,
					outputSourceFiles: true,
					sourceMapURL: 'bootstrap.css.map',
					sourceMapFilename: 'dist/css/bootstrap.css.map'
				},
				files: {
					'css/vendor/bootstrap.css': 'less/bootstrap.less'
				}
			},
			compileTheme: {
				options: {
					strictMath: true,
					sourceMap: true,
					outputSourceFiles: true,
					sourceMapURL: '<%= pkg.name %>-theme.css.map',
					sourceMapFilename: 'dist/css/<%= pkg.name %>-theme.css.map'
				},
				files: {
					'css/vendor/bootstrap-theme.css': 'less/theme.less'
				}
			},
			build: {
				files: {'css/EG.css': 'less/EG.less'}
			}
		},
		cssmin: {
			options: {
				banner: '/*<%=pkg.name%> <%=grunt.template.today("yyyy-mm-dd") %>*/'
			},
			combine: {
				src: ['css/EG.css', 'css/vendor/bootstrap.css', 'css/vendor/bootstrap-theme.css'],
				dest: 'build/prod/min.css/EG.css'
			} 
		},
		autoprefixer: {
			options: {
				browsers: [
					'Android 2.3',
					'Android >= 4',
					'Chrome >= 20',
					'Firefox >= 24', // Firefox 24 is the latest ESR
					'Explorer >= 8',
					'iOS >= 6',
					'Opera >= 12',
					'Safari >= 6'
          		]
      		},
      		core: {
      			options: {
      			map: true
      			},
      			src: 'css/vendor/bootstrap.css'
      		},
      		theme: {
      			options: {
      				map: true
      			},
      			src: 'css/vendor/bootstrap-theme.css'
      		}
      	},
    	watch: {
	      	options: {
	      		livereload:9090,
	      		nospawn: false
	      	},
	      	html: {
	      		files: 'index.html'				
	      	},
	      	scripts: {
	      		files: ['Gruntfile.js', 'js/EG.js'],
	      		tasks: 'jshint'	
	      	},
	      	less: {
	  			files: 'less/*.less',//['less/EG.less', 'less/bootstrap.less'],
	  			tasks: 'less'
	  		}
  		},  
  		htmlmin: {
  			options: {
		  		removeComments: true,
		  		removeCommentsFromCDATA: true,
		  		collapseWhitespace: true,
				//conservativeCollapse: true,
				//preserveLineBreaks: true,
				collapseBooleanAttributes: true,
				removeAttributeQuotes: true,
				removeRedundantAttributes: true,				
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true
			},
			files: {
				src: 'index.html',
				dest: 'build/prod/index.html'
			}
		}
});

	//'copy' loader
	grunt.loadNpmTasks('grunt-contrib-copy');

	//'cssmin' loader
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	//'htmlmin' loader
	grunt.loadNpmTasks('grunt-contrib-htmlmin');

	//'autoprefixer' loader
	grunt.loadNpmTasks('grunt-autoprefixer');

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Jshint loader
	grunt.loadNpmTasks('grunt-contrib-jshint');

	// 'watch' loader
	grunt.loadNpmTasks('grunt-contrib-watch');

	// LESS compiler loader
	grunt.loadNpmTasks('grunt-contrib-less');

	// Define task(s).
	grunt.registerTask('default', 'watch');
	grunt.registerTask('build', ['autoprefixer', 'htmlmin', 'uglify', 'cssmin', 'less', 'copy']);
};